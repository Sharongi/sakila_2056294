<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //Accion: meotodo del controlador, contiene el codigo a ejecutar
    //Nombre puede ser cualquiera
    //recomendado el nombre en minuscula
    public function index(){
        //seleccionar las categorias existentes
        $categorias = Categoria::paginate(5);        
        //recorrer cada categoria
        //enviar la coleccion de categorias a una vista
        //y las vamos a mostrar alli
        return view ("categorias.index")->with("categorias" , $categorias);
    }

    public function create(){
        //mostrar formulario crear categoria
        //echo "Formulario de Categoria";
        return view('categorias.new');
    }

    public function store(Request $r){

        //Validacion
        //1. Establecer reglas de validacion para cada campo
        $reglas = [
            "categoria" => ["required", "alpha"]
        ];

        $mensajes = [
            "required" => "Campo obligatorio",
            "alpha" => "Solo letras"
        ];

        //2. Crear el objeto validador
        $validador = Validator::make($r->all(), $reglas, $mensajes );

        //3. Validar : metodo fails
        //metodo fails : retorna true(v) si la validacion falla
        //retorna falso en caso de que los datos sean correctos
        if ($validador->fails()){
            //codigo para cuando falla
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo cuando la validacion es correcta
        }
        //llegar datos desde el formulario
        //guardar la categoria en BD
        //almacena la informacion que viene desde formularios
        
        //crear nueva categoria
        $categoria = new Categoria();
        //asignar nombre desde formulario
        $categoria->name = $r->input("categoria");
        //guardar la nueva categoria
        $categoria->save();
        //redireccion con datos de sesion
        return redirect("categorias/create")->with("mensaje" , "Categoria guardada");

    }

    public function edit($category_id){
        //Seleccionar categoria a editar
        $categoria = Categoria::find($category_id);

        //mostrar la vista de actualizacion de categoria
        //llevando dentro de la categoria
        return view("categorias.edit")->with("categoria" , $categoria);
    
    }

    public function update($category_id){
        //Seleccionar categoria a editar
        $categoria = Categoria::find($category_id);
        //editar sus atributos
        $categoria->name = $_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //retornar a formulario de edit
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria editada de manera correcta");

    }
}
